package com.udemykurs1.demo.UI.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDetailRequestModel {
    //Mit @NotNull wird validiert das dass Feld nicht leer sein darf dazu muss Hibernate Validation als Dependencie drin sein
    @NotNull(message="First name cannot be null")
    private String firstName;

    @NotNull(message="last name cannot be null")
    private String lastName;

    @NotNull(message="Email name cannot be null")
    @Email
    private String email;

    @Size(min=8, max=16, message="Password name cannot be null, or bust be longer than 8")
    private String password;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
