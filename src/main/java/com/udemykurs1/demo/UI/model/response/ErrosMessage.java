package com.udemykurs1.demo.UI.model.response;

import java.util.Date;

public class ErrosMessage {
    private Date timestamp;
    private String message;

    public ErrosMessage() {
    }

    public ErrosMessage(Date timestamp, String message) {
        this.timestamp = timestamp;
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
