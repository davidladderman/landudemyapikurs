package com.udemykurs1.demo.UI.model.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateUserDetailRequestModel {
    //Mit @NotNull wird validiert das dass Feld nicht leer sein darf dazu muss Hibernate Validation als Dependencie drin sein
    @NotNull(message="First name cannot be null")
    private String firstName;

    @NotNull(message="last name cannot be null")
    private String lastName;


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
