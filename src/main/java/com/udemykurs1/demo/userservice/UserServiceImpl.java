package com.udemykurs1.demo.userservice;


import com.udemykurs1.demo.UI.model.request.UserDetailRequestModel;
import com.udemykurs1.demo.UI.model.response.UserRest;
import com.udemykurs1.demo.shared.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service //erschaft im Spring container ein Bean für Dependcie Injection
public class UserServiceImpl implements UserService{
    Map<String, UserRest> users;
    Utils utils;
    
    public  UserServiceImpl(){

    }

    @Autowired
    public UserServiceImpl(Utils utils){
        this.utils = utils;
    }


    @Override
    public UserRest createUser(UserDetailRequestModel userDetails) {
        UserRest returnValue = new UserRest();
        returnValue.setEmail(userDetails.getEmail());
        returnValue.setFirstName(userDetails.getFirstName());
        returnValue.setLastName(userDetails.getLastName());
        //generiert eine uuid die dann weiter gegeben wird als response
        String userId = utils.generateUserID();
        returnValue.setUserID(userId);
        //erst
        if(users == null){
            users = new HashMap<>();
            users.put(userId,returnValue);
        }
        return returnValue;
    }
}
