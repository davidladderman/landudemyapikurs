package com.udemykurs1.demo.userservice;

import com.udemykurs1.demo.UI.model.request.UserDetailRequestModel;
import com.udemykurs1.demo.UI.model.response.UserRest;

public interface UserService {
    UserRest createUser(UserDetailRequestModel userDetails);
}
