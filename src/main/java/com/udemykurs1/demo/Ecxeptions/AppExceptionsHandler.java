package com.udemykurs1.demo.Ecxeptions;
import com.udemykurs1.demo.UI.model.response.ErrosMessage;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

//braucht dies anotations um den fehlern zuzuhören
@ControllerAdvice
public class AppExceptionsHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = {Exception.class})
    public ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request){
        String errorMessage = ex.getLocalizedMessage();
        if(errorMessage == null) errorMessage = ex.toString();
        ErrosMessage errosMessage = new ErrosMessage(new Date(), errorMessage);
        return new ResponseEntity<>(
                errosMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    //Mehr spezifische Exception Null Pointer Exceptions und eigene User Service Exception
    @ExceptionHandler(value = {NullPointerException.class, UserServiceException.class})
    public ResponseEntity<Object> handleSpecificException(Exception ex, WebRequest request){
        String errorMessage = ex.getLocalizedMessage();
        if(errorMessage == null) errorMessage = ex.toString();
        ErrosMessage errosMessage = new ErrosMessage(new Date(), errorMessage);
        return new ResponseEntity<>(
                errosMessage, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }



}
