package com.udemykurs1.demo.Ecxeptions;

import java.io.Serializable;

public class UserServiceException extends RuntimeException implements Serializable {


    private static final long serialVersionUID = 5029687038639869043L;

    public UserServiceException(String message){
        super(message);
    }
}
