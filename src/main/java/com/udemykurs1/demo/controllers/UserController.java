package com.udemykurs1.demo.controllers;

import com.udemykurs1.demo.Ecxeptions.UserServiceException;
import com.udemykurs1.demo.UI.model.request.UpdateUserDetailRequestModel;
import com.udemykurs1.demo.UI.model.request.UserDetailRequestModel;
import com.udemykurs1.demo.UI.model.response.UserRest;
import com.udemykurs1.demo.userservice.UserService;
import com.udemykurs1.demo.userservice.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RestController
@RequestMapping("/users") //-> http://localhost:8080/users
public class UserController {
    //ALle gnerierten users werden in eine Map gespeichert
    Map<String, UserRest> users;
    @Autowired
    UserService userService;






    @GetMapping()
    public String getUser(//wenn bei user zwei Parameter hinzugefügt werden mit RequestParam. In der Url wird der erste Parameter mit ? nach der Url angegebn und der zweite mit &
            @RequestParam(value="page",defaultValue = "1") int page, //default setzt standard wert
            @RequestParam(value = "limit", defaultValue = "50") int limit,
            @RequestParam(value = "beschreibung",required = false) String beschreibung)// required=false macht es optional gibt wenn er nicht da ist null zurück
    {
        return "get user was called with page "+page+ " and limit " +limit +" mit Beschreibung: "+ beschreibung;
    }











    @GetMapping(path="/{userId}", produces = {
            MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    public ResponseEntity<UserRest> getUser(@PathVariable String userId) {
        //Ein JAva Object returnen
        //zuerst ein UserRest Model erzeugt
        //Werte gesetzt und zurück gegeben
        //Dependencie JAckson Dataformat hilft json in xml umwandeln
        /*UserRest returnValue = new UserRest();
        returnValue.setEmail("david@lol.at");
        returnValue.setFirstName("David");
        returnValue.setLastName("lol");
        */

        //Errors wurden getestet
        //if(true) throw new UserServiceException("A user service excep");
        //String firstName = null;
        //int firstNameLength = firstName.length();


        if(users.containsKey(userId)){
            return new ResponseEntity<UserRest>(users.get(userId), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<UserRest>(HttpStatus.NO_CONTENT);
        }

        //Response Enity kann mehrere Parameter mitnehmen Body, Http Stautus, Headeretc
    }

















    //Um beim Post Request was mitzugeben brauch es einen Requestbody
    //Mit MediaType wird angegeben was für Typ mit gegeben wird in dem Fall JSON
    //consumes sagt das es die daten die im request mitgegeben wird consumed
    //produces zeigt das es auch produzieren kann
    @PostMapping(consumes = {
            MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    }, produces = {
            MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    //Um zu Validieren das Firstname zB nicht leer sein darf braucht man Annotationen @Valid die müssen auch im RequestModel definiert sein. Wenn @Valid nicht gesetz sind werden auhc di validireungen in der Klasse nicht stattfinden
    public ResponseEntity<UserRest> createUser(@RequestBody @Valid UserDetailRequestModel userDetails /*, BindingResult result*/){
        //gibt die fehler aus
        /*List<FieldError> errors = result.getFieldErrors();
        for (FieldError error :errors){
            System.out.println(error.getDefaultMessage());
        }*/
        UserRest returnValue = userService.createUser(userDetails);
        //Response Enity kann mehrere Parameter mitnehmen Body, Http Stautus, Headeretc
        return new ResponseEntity<UserRest>(returnValue, HttpStatus.OK);
    }









    @PutMapping(path="/{userId}",consumes = {
            MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    }, produces = {
            MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE
    })
    public UserRest updateUser(@PathVariable String userId,@Valid @RequestBody UpdateUserDetailRequestModel userDetails){
        UserRest storedUserDetails = users.get(userId);
        storedUserDetails.setFirstName(userDetails.getFirstName());
        storedUserDetails.setLastName(userDetails.getLastName());
        users.put(userId, storedUserDetails);
        return storedUserDetails;
    }











    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id){
        users.remove(id);
        return ResponseEntity.noContent().build();
    }
}
